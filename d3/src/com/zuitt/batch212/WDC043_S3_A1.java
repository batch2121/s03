package com.zuitt.batch212;

import java.util.InputMismatchException;
import java.util.Scanner;

public class WDC043_S3_A1 {
    public static void main(String[] args) {

        try {
            Scanner input = new Scanner(System.in);
            int num;
            int factorialResult = 1;
            System.out.println("Input an integer whose factorial will be computed:");
            num = input.nextInt();

            if(num > 0){
                for(int i = 1; i <= num;i++){
                    factorialResult *= i;
                }
                System.out.println("The factorial of " + num + " is " + factorialResult);
            } else if (num < 0) {
                System.out.println("The factorial of " + num + " is not possible." );
            } else {
                System.out.println("The factorial of " + num + " is " + 1 );
            }

        } catch (Exception e){
            System.out.println("Invalid Input");
        }
    }

}
